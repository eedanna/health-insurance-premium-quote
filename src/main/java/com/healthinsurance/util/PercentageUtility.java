package com.healthinsurance.util;


import com.healthinsurance.model.CurrentHealth;
import com.healthinsurance.model.Habits;

// TODO: Auto-generated Javadoc
/**
 * The Class PercentageUtility.
 */
public class PercentageUtility {

	/** The instace. */
	private static PercentageUtility instace;

	/**
	 * Instantiates a new percentage utility.
	 */
	private PercentageUtility() {

	}

	/**
	 * Gets the single instance of PercentageUtility.
	 *
	 * @return single instance of PercentageUtility
	 */
	public static PercentageUtility getInstance() {
		if (instace == null) {
			instace = new PercentageUtility();
		}
		return instace;
	}

	/**
	 * Gets the total percentage.
	 *
	 * @param totalPremium the total premium
	 * @param includePercentage the include percentage
	 * @param ind the ind
	 * @return the total percentage
	 */
	public int getTotalPercentage(final int totalPremium, final int includePercentage, final String ind) {

		float percentage = totalPremium / 100 * includePercentage;
		float totalPercentage = 0;
		if ("increase".equalsIgnoreCase(ind)) {
			totalPercentage = totalPremium + percentage;
		} else {
			// reduce the percentage
			totalPercentage = totalPremium - percentage;
		}
		return (int) totalPercentage;
	}

	/**
	 * Gets the premium percentage by age.
	 *
	 * @param age the age
	 * @param standardPremium the standard premium
	 * @return the premium percentage by age
	 */
	public int getPremiumPercentageByAge(int age, final int standardPremium) {
		int finalPercentage = 0;
		if ((age >= 18 && age < 25) || (age >= 25 && age < 30) || (age >= 30 && age < 35) || (age >= 35 && age < 40)) {
			finalPercentage = getTotalPercentage(standardPremium, 30, "increase");
			age = age + 5;
		} else {
			finalPercentage = getTotalPercentage(standardPremium, 20, "increase");
			age = age + 5;
		}
		return finalPercentage;
	}

	/**
	 * Gets the premium percentage by gender.
	 *
	 * @param gender the gender
	 * @param calPremium the cal premium
	 * @return the premium percentage by gender
	 */
	public int getPremiumPercentageByGender(final String gender, final int calPremium) {
		int finalPercentage = 0;
		if ("Male".equalsIgnoreCase(gender)) {
			finalPercentage = getTotalPercentage(calPremium, 2, "increase");
		} else {
			finalPercentage = getTotalPercentage(calPremium, 0, "increase");
		}
		return finalPercentage;
	}

	/**
	 * Gets the premium percentage by current health.
	 *
	 * @param currentHealth the current health
	 * @param calPremium the cal premium
	 * @return the premium percentage by current health
	 */
	public int getPremiumPercentageByCurrentHealth(final CurrentHealth currentHealth, final int calPremium) {
		int finalPercentage = 0;
		if ("Hypertension".equalsIgnoreCase(currentHealth.getHypertension())
				|| "Blood pressure".equalsIgnoreCase(currentHealth.getBloodPressure())
				|| "Blood sugar".equalsIgnoreCase(currentHealth.getBloodSugar())
				|| "Overweight".equalsIgnoreCase(currentHealth.getOverweight())) {
			finalPercentage = getTotalPercentage(calPremium, 1, "increase");
		}
		return finalPercentage;
	}

	/**
	 * Gets the premium percentage by habits.
	 *
	 * @param habits the habits
	 * @param calPremium the cal premium
	 * @return the premium percentage by habits
	 */
	public int getPremiumPercentageByHabits(final Habits habits, final int calPremium) {
		int finalPercentage = 0;
		if ("Smoking".equalsIgnoreCase(habits.getSmoking()) || "Alcohol".equalsIgnoreCase(habits.getAlcohol())
				|| "Drugs".equalsIgnoreCase(habits.getDrugs())) {
			finalPercentage = getTotalPercentage(calPremium, 3, "increase");
		} else {
			// good habits (Daily exercise) - reduce 3%
			finalPercentage = getTotalPercentage(calPremium, 3, "reduce");
		}
		return finalPercentage;
	}
}
