package com.healthinsurance.util;

import com.healthinsurance.model.CurrentHealth;
import com.healthinsurance.model.Habits;

// TODO: Auto-generated Javadoc
/**
 * The Class Utils.
 */
public class Utils {

	/**
	 * Gets the current health.
	 *
	 * @param currentHealth the current health
	 * @return the current health
	 */
	public static void getCurrentHealth(final CurrentHealth currentHealth) {
		currentHealth.setHypertension((currentHealth.getHypertension().equalsIgnoreCase("Yes")) ? "Hypertension" : "");
		currentHealth
				.setBloodPressure((currentHealth.getBloodPressure().equalsIgnoreCase("Yes")) ? "Blood pressure" : "");
		currentHealth.setBloodSugar((currentHealth.getBloodSugar().equalsIgnoreCase("Yes")) ? "Blood sugar" : "");
		currentHealth.setOverweight((currentHealth.getOverweight().equalsIgnoreCase("Yes")) ? "Overweight" : "");
	}

	/**
	 * Gets the habits.
	 *
	 * @param habits the habits
	 * @return the habits
	 */
	public static void getHabits(final Habits habits) {
		habits.setSmoking((habits.getSmoking().equalsIgnoreCase("Yes")) ? "Smoking" : "");
		habits.setAlcohol((habits.getAlcohol().equalsIgnoreCase("Yes")) ? "Alcohol" : "");
		habits.setDailyExercise((habits.getDailyExercise().equalsIgnoreCase("Yes")) ? "Daily exercise" : "");
		habits.setDrugs((habits.getDrugs().equalsIgnoreCase("Yes")) ? "Drugs" : "");
	}

}
