<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
	"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Registration</title>
</head>
<body>
	<div align="center">
		<form:form action="register" method="post" commandName="userForm">
			<table border="0">
				<tr>
					<td colspan="2" align="center"><h2>Health Insurance Premium Quote </h2></td>
				</tr>
				<tr>
					<td>Name:</td>
					<td><form:input path="name" /></td>
				</tr>
				<tr>
					<td>Age :</td>
					<td><form:input path="age" /></td>
				</tr>
				<tr>
					<td>Gender:</td>
					<td><form:input path="gender" /></td>
				</tr>
				<tr>
					<td>Current health:</td>
				</tr>
				<tr>
					<td>Hypertension:</td>
					<td><form:select path="currentHealth.hypertension"  items="${yesnoList}" /></td>
				</tr>
				<tr>
					<td>Blood pressure:</td>
					<td><form:select path="currentHealth.bloodPressure" items="${yesnoList}" /></td>
				</tr>
				
				<tr>
					<td>Blood Sugar:</td>
					<td><form:select path="currentHealth.bloodSugar"  items="${yesnoList}" /></td>
				</tr>
				
				<tr>
					<td>Overweight:</td>
					<td><form:select path="currentHealth.overweight"  items="${yesnoList}" /></td>
				</tr>

				<tr>
					<td>Habits:</td>
				</tr>
				<tr>
					<td>Smoking:</td>
					<td><form:select path="habits.smoking" items="${yesnoList}" /></td>
				</tr>
				<tr>
					<td>Alcohol:</td>
					<td><form:select path="habits.alcohol"  items="${yesnoList}" /></td>
				</tr>				
				<tr>
					<td>Daily exercise:</td>
					<td><form:select path="habits.dailyExercise" items="${yesnoList}" /></td>
				</tr>				
				<tr>
					<td>Drugs:</td>
					<td><form:select path="habits.drugs"  items="${yesnoList}" /></td>
				</tr>
				<tr>
					<td colspan="2" align="center"><input type="submit"
						value="Calculate Premium Quote" /></td>
				</tr>
			</table>
		</form:form>
	</div>
</body>
</html>